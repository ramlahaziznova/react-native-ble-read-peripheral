var util = require('util');
var bleno = require('@abandonware/bleno');

const Characteristic = bleno.Characteristic;

const _storedMovement = 'START11:48:16/r1Y/r2N/r3N/r4N/r5Y/r6Y/r7N/r8Y/r9Y/r10Y/r11N/r12Y/r13N/r14N/r15Y/r16Y/rEND/r' // null;
var _updateValueCallback = null;

const movementCharacteristic = new Characteristic({
  uuid: '20000000-0000-0000-0000-000000000002',
  properties: ['read', 'notify'],
    descriptors: [
      new bleno.Descriptor({
        uuid: '2901',
        value: 'Check dummy baby movement'
      })
    ],

  // handle on read request
  onReadRequest : function(offset, callback) {
    console.log('incoming read request')

    if (offset) {
      callback(Characteristic.RESULT_ATTR_NOT_LONG, null);
      // return;
    } else {
      // get data, then write response
      var data = _storedMovement ? _storedMovement.toString() : 'you have no movement stored';
      console.log('getting movement:', data);
      callback(Characteristic.RESULT_SUCCESS, Buffer.from(data, 'utf8'));
    }
  },

  // handle on subscribe
  onSubscribe : function(maxValueSize, updateValueCallback) {
    console.log('on subscribe');

    _updateValueCallback = updateValueCallback;
  },

  // handle on unsubscribe
  onUnsubscribe : function() {
    console.log('on unsubscribe');

    _updateValueCallback = null;
  }
})

module.exports = movementCharacteristic;

/** Deprecated format 
// prepare movement characteristic
function MovementCharacteristic() {
  bleno.Characteristic.call(this, {
    uuid: '20000000-0000-0000-0000-000000000002'.replace(/\-/gi, ""),
    properties: ['read', 'notify'],
    descriptors: [
      new bleno.Descriptor({
        uuid: '2901',
        value: 'Check dummy baby movement'
      })
    ]
  });

  this._storedMovement = 'START11:48:16/r1Y/r2N/r3N/r4N/r5Y/r6Y/r7N/r8Y/r9Y/r10Y/r11N/r12Y/r13N/r14N/r15Y/r16Y/rEND/r' // null;
  this._updateValueCallback = null;
}

util.inherits(MovementCharacteristic, bleno.Characteristic);

// handle on read request
MovementCharacteristic.prototype.onReadRequest = function(offset, callback) {
  console.log('incoming read request')

  if (offset) {
    callback(this.RESULT_ATTR_NOT_LONG, null);
    return;
  }

  // get data, then write response
  var data = this._storedMovement ? this._storedMovement.toString() : 'you have no movement stored';
  console.log('getting movement:', data);
  callback(this.RESULT_SUCCESS, Buffer.from(data, 'utf8'));
};

// handle on subscribe
MovementCharacteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('on subscribe');

  this._updateValueCallback = updateValueCallback;
};

// handle on unsubscribe
MovementCharacteristic.prototype.onUnsubscribe = function() {
  console.log('on unsubscribe');

  this._updateValueCallback = null;
};

module.exports = MovementCharacteristic;
*/