var bleno = require('@abandonware/bleno');

var MovementService = require('./movement-service');

var name = 'Movement';

// wait until the BLE radio powers on before attempting to advertise.
// if BLE radio is unavailable, then this event will never be called.
bleno.on('stateChange', function (state) {
  if (state === 'poweredOn') {

    // start to advertise service id, making it easie to find.
    bleno.startAdvertising(name, [MovementService.uuid], function (err) {
      if (err) {
        console.log('start advertising error', err);
      }
    });

    // return;
  }
  else{
    bleno.stopAdvertising();

  }

});

// handle on advertising start event
bleno.on('advertisingStart', function (err) {
  if (err) {
    console.log('start advertising error', err);
    return;
  }
  
  // once we are advertising, it's time to set up our services,
  // along with our characteristics.
  console.log('advertising...');
  bleno.setServices([
    MovementService
  ]);
});


// var name = 'Movement';
// var movementService = new MovementService();

// // wait until the BLE radio powers on before attempting to advertise.
// // if BLE radio is unavailable, then this event will never be called.
// bleno.on('stateChange', function (state) {
//   if (state === 'poweredOn') {

//     // start to advertise service id, making it easie to find.
//     bleno.startAdvertising(name, [movementService.uuid], function (err) {
//       if (err) {
//         console.log('start advertising error', err);
//       }
//     });

//     return;
//   }

//   bleno.stopAdvertising();
// });

// // handle on advertising start event
// bleno.on('advertisingStart', function (err) {
//   if (err) {
//     console.log('start advertising error', err);
//     return;
//   }
  
//   // once we are advertising, it's time to set up our services,
//   // along with our characteristics.
//   console.log('advertising...');
//   bleno.setServices([
//     movementService
//   ]);
// });
