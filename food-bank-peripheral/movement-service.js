var util = require('util');
var bleno = require('@abandonware/bleno');

var MovementCharacteristic = require('./movement-characteristic');

var PrimaryService = bleno.PrimaryService;

// create movement service, then register movement characteristic to it
const MovementService = new PrimaryService({
    uuid: '90000000-0000-0000-0000-000000000009'.replace(/\-/gi, ""),
    characteristics: [
        MovementCharacteristic,
    ]
})

module.exports = MovementService;


// // create movement service, then register movement characteristic to it
// function MovementService() {
//     bleno.PrimaryService.call(this, {
//         uuid: '90000000-0000-0000-0000-000000000009'.replace(/\-/gi, ""),
//         characteristics: [
//             new MovementCharacteristic(),
//         ]
//     });
// }

// util.inherits(MovementService, bleno.PrimaryService);

// module.exports = MovementService;
